from django.contrib import admin
from .models import Task, Trash
# Register your models here.

admin.site.register(Task)

admin.site.register(Trash)