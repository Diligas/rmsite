# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-06-10 10:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('srm', '0006_auto_20170610_0948'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='rm_file',
            field=models.FilePathField(allow_folders=True, path='/home', recursive=True),
        ),
    ]
