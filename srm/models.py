from __future__ import unicode_literals

from django.db import models


# Create your models here.


class Trash(models.Model):
    path = models.FilePathField(allow_files=False, allow_folders=True, path="/home", recursive=True)
    status = models.CharField(max_length=100, default="")
    recursive = models.BooleanField(default=False)
    interactive = models.BooleanField(default=False)
    directory = models.BooleanField(default=False)
    force = models.BooleanField(default=False)
    verbose = models.BooleanField(default=False)
    silent = models.BooleanField(default=False)
    dry_run = models.BooleanField(default=False)
    name_valid = models.CharField(max_length=20, default="error")

    def __str__(self):
        return self.path


class Task(models.Model):
    name = models.CharField(max_length=100, default="", unique=True)
    trash = models.ForeignKey(Trash)
    rm_file = models.FilePathField(allow_files=True, allow_folders=True, recursive=True, path="/home")
    status = models.CharField(max_length=100, default="")
    recursive = models.BooleanField(default=False)
    interactive = models.BooleanField(default=False)
    directory = models.BooleanField(default=False)
    force = models.BooleanField(default=False)
    verbose = models.BooleanField(default=False)
    silent = models.BooleanField(default=False)
    dry_run = models.BooleanField(default=False)
    name_valid = models.CharField(max_length=20, default="error")

    def __str__(self):
        return self.name
