import os

from django.http import HttpResponseNotFound
from django.shortcuts import render
from django.core.urlresolvers import reverse, reverse_lazy

# Create your views here.
from django.views.generic import CreateView, ListView, DetailView

from srm.forms import TaskCreationForm
from srm.models import Task, Trash


def get_files(request):

    if request.method == 'GET':
        path = '/home'
    elif request.method == 'POST':
        path = request.POST['path']
    else:
        return HttpResponseNotFound('<h1>Page not found</h1>')

    files = []
    for item in os.listdir(path):
        path_to_item = os.path.join(path, item)
        file_rm = {'name': item,
                   'path': path_to_item,
                   'is_dir': os.path.isdir(path_to_item)
                   }
        files.append(file_rm)

    files.sort(key=lambda o: o['name'])
    return render(request, 'srm/files_list.html', {'files': files})


class TaskCreate(CreateView):
    model = Task
    form_class = TaskCreationForm
    template_name = 'srm/tasks/task_create.html'
    success_url = reverse_lazy('srm:files_list')

    def form_valid(self, form):
        form.instance.path = self.request.GET['path']
        print form.instance.path
        return super(TaskCreate, self).form_valid(form)


class TaskListView(ListView):
    model = Task
    template_name = 'srm/tasks/task_list.html'


class TaskDetailView(DetailView):
    model = Task
    template_name = 'srm/tasks/task_detail.html'


class TrashListView(ListView):
    model = Trash
    template_name = 'srm/trash/tash_list.html'
