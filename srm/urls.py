from django.conf.urls import url

from srm import views

urlpatterns = [
    url(r'^$', views.get_files),
    url(r'^files/', views.get_files, name='files_list'),
    url(r'^task/create', views.TaskCreate.as_view(), name='task_create'),
    url(r'^tasks', views.TaskListView.as_view(), name='tasks'),
    url(r'^task/(?P<pk>\d+)/$', views.TaskDetailView.as_view(), name='task_details'),
    url(r'^trash/', views.TrashListView.as_view(), name='trash'),
]
