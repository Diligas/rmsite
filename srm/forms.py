from django import forms
from .models import Task


class TaskCreationForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ['rm_file','status']